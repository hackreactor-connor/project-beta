import React, { useState, useEffect } from "react";


function SalePersonSalesList() {
    const [state, setState] = useState({
        id: ''
    });
    const [filteredSales, setFilteredSales] = useState([])
    const [sellers, setSellers] = useState([])


    useEffect(() => {
        const fetchSalePersonData = async () => {
            const resSalePerson = await fetch('http://localhost:8090/api/list_sales_persons');
            const salePersonData = await resSalePerson.json();
            setSellers(salePersonData);
        }
        fetchSalePersonData();


        const fetchFilteredRecords = async () => {
            const res = await fetch(`http://localhost:8090/api/list_sale_records/${state.id}`);
            const saleRecordData = await res.json();
            const arr = saleRecordData['sale_records'];

            setFilteredSales(arr);
        };
        fetchFilteredRecords();
    }, [state])



    const handleChange = event => {
        const value = event.target.value;
        setState({
            ...state,
            [event.target.name]: value,
        });
    };




    return (
        <>
            <form>
                <select
                    onChange={handleChange}
                    value={state.id}
                    required
                    name='id'
                    id='id'
                >
                    <option value=''>Choose a sales person</option>
                    {sellers.map(salesPerson => {
                        return (
                            <option
                                key={salesPerson.id}
                                value={salesPerson.id}
                            >
                                {salesPerson.name}
                            </option>
                        );
                    })}
                </select>
            </form>

            <table className='table table-striped table-hover'>
                <thead>
                    <tr>
                        <th>Seller Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales.map(record => {
                        return (
                            <tr key={record.id}>
                                <td>{record.sales_person.name}</td>
                                <td>{record.customer.name}</td>
                                <td>{record.automobile.vin}</td>
                                <td>{record.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table >
        </>
    );
}
export default SalePersonSalesList;
