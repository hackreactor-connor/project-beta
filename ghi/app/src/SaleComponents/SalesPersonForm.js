import React, { useState } from "react";

function SalesPersonForm() {
    const [state, setState] = useState({
        name: '',
        employee_number: '',
    });
    const [successfulSubmit, setSuccessfulSubmit] = useState(false);

    let formClasses = "";
    let alertClasses = "alert alert-success d-none mb-3";
    let alertContainerClasses = "d-none";

    const handleSubmit = async event => {
        event.preventDefault();
        const data = state
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const res = await fetch("http://localhost:8090/api/list_sales_persons", fetchConfig);

        if (res.ok) {
            setState({
                name: '',
                employee_number: '',
            });
            setSuccessfulSubmit(true);
        }
    };



    const handleChange = event => {
        const value = event.target.value;
        setState({
            ...state,
            [event.target.name]: value,
        });
    };



    if (successfulSubmit) {
        formClasses = "d-none";
        alertClasses = "alert alert-success d-none mb-3";
        alertContainerClasses = "";
    }

    return (
        <div className='row'>
            <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                    <h1>Create New Sales Person</h1>
                    <form
                        onSubmit={handleSubmit}
                        id='create-sale-person-form'
                        className={formClasses}>

                        <div className='form-floating mb-3'>
                            <input
                                onChange={handleChange}
                                value={state.name}
                                placeholder='sellers name'
                                required
                                name='name'
                                id='sale_person_name'
                                className='form-control'
                            />
                            <label htmlFor='style_name'>Name</label>
                        </div>

                        <div className='form-floating mb-3'>
                            <input
                                onChange={handleChange}
                                value={state.employee_number}
                                placeholder='Employee Number'
                                required
                                name='employee_number'
                                id='sale_person_number'
                                className='form-control'
                            />
                            <label htmlFor='color'>employee number</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>

                    </form>

                    <div className={alertContainerClasses}>
                        <div className={alertClasses} id='success-message'>
                            Sales Person created successfully
                        </div>
                        <button
                            onClick={() => setSuccessfulSubmit(false)}
                            className='btn btn-primary'
                        >
                            Create another Sales Person
                        </button>
                    </div>

                </div>
            </div>
        </div>
    )

}
export default SalesPersonForm;
