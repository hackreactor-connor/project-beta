import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid" id="work">
        <NavLink className="navbar-brand" to="/">
          Harry & Connor
        </NavLink>
        <div className="p-2 dropdown">
          <NavLink
            className="btn btn-secondary dropdown-toggle"
            to="#"
            role="button"
            id="dropdownMenuLink"
            data-bs-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Inventory
          </NavLink>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <NavLink className="dropdown-item" to="/manufacturers/">
              Show Manufacturers
            </NavLink>
            <NavLink className="dropdown-item" to="/manufacturers/create/">
              Create Manufacturer
            </NavLink>
            <NavLink className="dropdown-item" to="/automobiles/">
              Show Automobiles
            </NavLink>
            <NavLink className="dropdown-item" to="/automobiles/create/">
              Create Automobiles
            </NavLink>
            <NavLink className="dropdown-item" to="/vehicles/">
              Show Vehicle Models
            </NavLink>
            <NavLink className="dropdown-item" to="/vehicles/create/">
              Create Vehicle Model
            </NavLink>
          </div>
        </div>
        <div className="p-2 dropdown">
          <NavLink
            className="btn btn-secondary dropdown-toggle"
            to="#"
            role="button"
            id="dropdownMenuLink"
            data-bs-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Services
          </NavLink>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <NavLink className="dropdown-item" to="/technicians/">
              Create Technician
            </NavLink>
            <NavLink className="dropdown-item" to="/appointments/">
              Show Appointments
            </NavLink>
            <NavLink className="dropdown-item" to="/appointments/create/">
              Create Appointment
            </NavLink>
            <NavLink className="dropdown-item" to="/appointments/history/">
              Show Appointment History
            </NavLink>
          </div>
        </div>

        <div className="p-2 dropdown">
          <NavLink
            className="btn btn-secondary dropdown-toggle"
            to="#"
            role="button"
            id="dropdownMenuLink"
            data-bs-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sales
          </NavLink>
          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <NavLink className="dropdown-item" to="/sales/create-seller/">
              Create Sales Person
            </NavLink>
            <NavLink className="dropdown-item" to="customers/">
              Create Customer
            </NavLink>
            <NavLink className="dropdown-item" to="sales/list/">
              Show All Sales
            </NavLink>
            <NavLink className="dropdown-item" to="sales/create/">
              Make Sale
            </NavLink>
            <NavLink className="dropdown-item" to="sales/seller-list/">
              Show Sales by Seller
            </NavLink>
          </div>
        </div>



        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item"></li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
